
function _base64ToArrayBuffer(base64) {
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

var _XMLHttpRequest = XMLHttpRequest;
var XMLHttpRequestOverride = function() {
	//this._parent = _XMLHttpRequest;
	//this._parent();

	this._pending_data = null;

	this.onload = function() {}
	this.onerror = function() {}

	this._open = this.open;
	this.open = function(request, url) {
		this._pending_data = null;
		if (request == "GET") {
			for (var name in overrides) {
				if (url.endsWith(name)) {
					// alert("hooked url " + url)
					this._pending_data = overrides[name]
					return;
				}
			}
		}
		alert("bypassed url:" + url)
		return this._open(request, url);
	}

	this.send = function() {
		if (this._pending_data) {
			if (this.responseType == "text") {
				this.responseText = this.response = atob(this._pending_data)
				// alert("hooked data: " + this.responseText)
					
			} else {
				this.response = _base64ToArrayBuffer(this._pending_data);
			}

			this.onload();
			this._invoke("progress")
			this._invoke("load")
			// alert("called onload " + this.onload)
		}
	}

	this._events = {};

	this.addEventListener = function(name, event) {
		this._events[name] = this._events[name] || [];
		this._events[name].push(event);
	}

	this._invoke = function(name) {
		if (this._events[name]) {
			for (var i in this._events[name]) {
				this._events[name][i]();
			}
		}
	}
}
