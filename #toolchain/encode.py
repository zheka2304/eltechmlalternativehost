import base64

def encode_file(f):
	with open(f, "rb") as image_file:
	    encoded_string = base64.b64encode(image_file.read())
	    print(encoded_string)


def make_override_script(overrides):
	with open("override_hook.js", "r") as override_hook_script:
		override_hook = override_hook_script.read()

	result = ""
	for override, file in overrides.items():
		with open(file, "rb") as f:
			result += '    "' + override + '":"' + str(base64.b64encode(f.read()))[2:-1] + '", \n'
	result = "var overrides = {\n" + result + "\n};\n\n" + override_hook

	return result


def build(path, result):
	script = make_override_script({
		"Build/Build.json": "../Build/Build.json",
		"Build/Build.data.unityweb": "../Build/Build.data.unityweb",
		"Build/Build.wasm.code.unityweb": "../Build/Build.wasm.code.unityweb",
		"Build/Build.wasm.framework.unityweb": "../Build/Build.wasm.framework.unityweb",
	})

	with open("template_html.html", "r") as f:
		template = f.read()
	template = template.replace("###LABEL###", script)

	with open(result, "w") as f:
		f.write(template)


build("../", "../build.html")